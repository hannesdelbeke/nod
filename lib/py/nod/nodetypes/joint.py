import maya.cmds as mc

from nod.decorators.property import DelayedProperty

from nod.channels.base import NodChannel
from nod.channels.rotateorder import NodChannelRotateOrder

from nod.nodetypes.transform import NodTransform


class NodJoint(NodTransform):

    def __init__(self, *args):
        super(NodJoint, self).__init__(*args)

    @DelayedProperty
    def orient(self):
        return NodChannel(self, 'jointOrient')

    @DelayedProperty
    def orientX(self):
        return NodChannel(self, 'jointOrientX')

    @DelayedProperty
    def orientY(self):
        return NodChannel(self, 'jointOrientY')

    @DelayedProperty
    def orientZ(self):
        return NodChannel(self, 'jointOrientZ')

    @DelayedProperty
    def rotateOrder(self):
        """
        Special NodChannel that allows setting rotateOrder attributes using strings rather than ints

        Returns:
            nod.channels.rotateOrder.NodChannelRotateOrder
        """

        return NodChannelRotateOrder(self)

    def degreesOfFreedom(self, x=True, y=True, z=True):
        """
        Method to set degrees of freedom for each axis

        Args:
            x (boo): Toggle dof on x axis
            y (boo): Toggle dof on y axis
            z (boo): Toggle dof on z axis

        Returns:

        """
        for xyz, value in zip('xyz', (x, y, z)):
            NodChannel(self, 'jointType{}'.format(xyz.upper())).set(value)

    def matchTo(self, other):
        """
        Matches the current joint with another joint's properties:
            * Position
            * Orientation
            * RotateOrder

        Args:
            other (NodJoint): Joint to match to
        """
        mc.delete(mc.pointConstraint(str(other), self.name()))
        mc.delete(mc.orientConstraint(str(other), self.name()))
        self.freeze()

        self.rotateOrder.set(other.rotateOrder.get())
        self.stiffness.set(*other.stiffness.get())
        self.preferredAngle.set(*other.preferredAngle.get())

