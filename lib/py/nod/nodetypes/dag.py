import maya.cmds as mc
import maya.api.OpenMaya as om

from nod.decorators.property import DelayedProperty
from nod.nodetypes.base import NodBase
from nod.channels.base import NodChannel

from nod.nodetypes.base import Nod


class NodDag(NodBase):

    def __init__(self, *args):
        super(NodDag, self).__init__(*args)

    @DelayedProperty
    def dagPath(self):
        """
        Get dagPath object for the current node

        Returns:
            maya.api.OpenMaya.MDagPath
        """
        selList = om.MGlobal.getSelectionListByName(self.name())
        return selList.getDagPath(0)

    @DelayedProperty
    def v(self):
        """
        Visibility channel

        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'v')

    @DelayedProperty
    def visibility(self):
        """
        Visibility channel

        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'visibility')

    def hide(self, lock=True):
        """
        Hide Node

        Args:
            lock (bool): If True lock visibility channel
        """
        self.v.unlock()
        self.v.set(0)
        if not lock:
            return

        self.v.lock()

    def unhide(self):
        """
        Unhide node. Unlocks visibility channel if locked.
        """
        self.v.unlock()
        self.v.set(1)

    def setParent(self, parent, **kwargs):
        """
        Parent current node under the given parent
        Using maya.cmds.parent() method arguments

        Args:
            parent (str/nod.nodetypes.base.NodBase): Parent
        """
        currentParent = self.parent()
        if currentParent and currentParent == Nod(parent):
            print '"{}" is already parented under "{}"'.format(self, parent)
            return

        mc.parent(self.name(), str(parent), **kwargs)
