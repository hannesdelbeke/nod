import maya.cmds as mc
import maya.api.OpenMaya as om

from nod.decorators.property import DelayedProperty
from nod.nodetypes.shape import NodShape


class NodMesh(NodShape):
    def __init__(self, *args):
        super(NodMesh, self).__init__(*args)

    @DelayedProperty
    def fn(self):
        """
        Get MFnMesh object for the current mesh

        Returns:
            maya.api.OpenMaya.MFnMesh
        """
        return om.MFnMesh(self.dagPath)
