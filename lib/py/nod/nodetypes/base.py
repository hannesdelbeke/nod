import maya.api.OpenMaya as om
import maya.cmds as mc

from nod.channels.base import NodChannel


def Nod(name, typ=None, mute=False):
    """
    Cast the given name into a Nod object (Nod / NodChannel)

    Args:
        name (str or nod.nodetypes.base.NodBase or nod.channels.base.NodChannel): Name of the node or node.attribute
        typ (str): Maya node type
        mute (bool): If True do not raise when node is not found. Simply return None 

    Returns:
        nod.nodetypes.base.NodBase or nod.nodetypes.base.NodBase or nod.channels.base.NodChannel
    """
    if isinstance(name, (NodBase, NodChannel)):
        return name

    # This will accept both pymel and maya.cmds nodes
    name = str(name)
    if '.' in name:
        name, attributeName = name.split('.', 1)
        return Nod(name, typ=typ, mute=mute).attr(attributeName)

    if not mute:
        klass = NodeTypeClassFactory.byName(name, typ=typ)
    else:
        try:
            klass = NodeTypeClassFactory.byName(name, typ=typ)
        except Exception:
            return None

    if not typ:
        return klass(name)

    results = mc.ls(name, typ=typ)
    _raiseIfNotSingleNode(results, name)

    return klass(results[0])

def NodByUuid(uuid):
    klass = NodeTypeClassFactory.byUuid(uuid)
    return klass(uuid)


def _raiseIfNotSingleNode(nodes, identifier):
    if not nodes:
        raise NameError('Node "{}" NOT found.'.format(identifier))

    if len(nodes) > 1:
        raise NameError('More than 1 node found with uuid "{}". Provide full path'.format(identifier))


def _nameByUuid(uuid):

    names = mc.ls(uuid, uuid=False)
    _raiseIfNotSingleNode(names, uuid)

    return names[0]

def _uuidByName(name):

    uuids = mc.ls(name, uuid=True)
    _raiseIfNotSingleNode(uuids, name)

    return uuids[0]


class NodeTypeClassFactory(object):

    def __init__(self):
        pass

    @classmethod
    def byType(cls, nodeType):
        """
        Get Node by maya nodeType

        Args:
            nodeType (str): Maya node type name

        Returns:
            nod.nodetypes.base.NodBase
        """
        if nodeType == 'transform':
            from nod.nodetypes.transform import NodTransform
            return NodTransform
        elif nodeType == 'joint':
            from nod.nodetypes.joint import NodJoint
            return NodJoint
        elif nodeType == 'ikHandle':
            from nod.nodetypes.ikhandle import NodIkHandle
            return NodIkHandle
        elif nodeType == 'mesh':
            from nod.nodetypes.mesh import NodMesh
            return NodMesh
        elif nodeType.endswith('Constraint'):
            from nod.nodetypes import constraint
            if nodeType == 'aimConstraint':
                return constraint.NodAimConstraint
            elif nodeType == 'orientConstraint':
                return constraint.NodOrientConstraint
            elif nodeType == 'parentConstraint':
                return constraint.NodParentConstraint
            elif nodeType == 'pointConstraint':
                return constraint.NodPointConstraint
            elif nodeType == 'poleVectorConstraint':
                return constraint.NodPoleVectorConstraint
            elif nodeType == 'scaleConstraint':
                return constraint.NodScaleConstraint
            else:
                return constraint.NodConstraint
        elif nodeType == 'blendShape':
            from nod.nodetypes.blendShape import NodBlendShape
            return NodBlendShape
        elif nodeType == 'nonLinear':
            from nod.nodetypes.nonLinear import NodNonLinear
            return NodNonLinear
        elif nodeType == 'effector':
            from nod.nodetypes.ikhandle import NodEffector
            return NodEffector

        else:
            return NodBase

    @classmethod
    def byName(cls, name, typ=None):
        """
        Get Node by scene name
        """
        if typ:
            return cls.byType(typ)
        return cls.byType(mc.nodeType(name))

    @classmethod
    def byUuid(cls, uuid):
        """
        Get Node by maya node uuid
        """
        name = _nameByUuid(uuid)
        return cls.byType(mc.nodeType(name))


class NodBase(object):

    def __init__(self, nameOrUuid):
        self._uuid = self.uuid(nameOrUuid)

    @classmethod
    def uuid(cls, name):
        """
        Returns:
            (str): Uuid of the current node
        """
        # TODO: regex and raise if invalid
        if '-' in name:
            return name

        return _uuidByName(name)

    def name(self, short=False):
        """
        Args:
            short (bool): If True return short name not a full path

        Returns:
            (str): Name of the node by the current Nod uuid
        """
        names = mc.ls(self._uuid, sn=short)
        if not names:
            raise RuntimeError('Node "{}" no longer exists.'.format(self._uuid))

        assert(len(names) == 1)

        if short:
            names = [n.split('|')[-1] for n in names]

        return names[0]

    def __getattr__(self, attr):
        try:
            return getattr(super(NodBase, self), attr)
        except AttributeError:
            try:
                return self.attr(attr)
            except RuntimeError, e:
                raise AttributeError, 'Node "{}" has no attribute or method "{}"'.format(self, attr)

    def __repr__(self):
        return '<{} "{}">'.format(self.__class__, self.name())

    def __str__(self):
        return self.name()

    def __bool__(self):
        return bool(mc.ls(self._uuid))

    def __eq__(self, other):
        return self._uuid == other._uuid

    def __contains__(self, item):
        return item in self.name()

    def __add__(self, other):
        """
        If a string is added to the Nod a new Nod object is created using the following logic:
            Nod(name) + '_suffix'
                results in:
            Nod(name_suffix)
            
            Nod(name) + '.attribute'
                results in:
            NodChannel(name.attribute)

        Args:
            other (string): Suffix name

        Returns:
            nod.channels.base.NodChannel/nod.nodetypes.base,NodBase: channel or node
        """
        if isinstance(other, basestring):
            return Nod(str(self) + other)

    def addAttr(self, *args, **kwargs):
        """
        Utilizing maya.cmds.addAttr() 

        Returns:
            nod.channels.base.NodChannel
        """
        mc.addAttr(str(self), *args, **kwargs)

        attrName = kwargs.get('longName', kwargs.get('ln', kwargs.get('shortName', kwargs.get('sn', None))))
        if attrName is None:
            return

        try:
            channel = self.attr(attrName)
        except RuntimeError:
            # Parent attributes are not queryable
            pass
        else:
            return channel

    def attr(self, name):
        return NodChannel(self, name.lstrip('.'))

    def attrExists(self, attributeName):
        """
        Returns True if the node has an attribute
        Args:
            attributeName (str): Attribute name 

        Returns:
            (bool): True if node has an attribute with the given name
        """
        return mc.objExists('{}.{}'.format(self.name(), attributeName))

    def rename(self, name=None, replace=None):
        """
        Rename the node and cast the new name.
        Relies on node ID so no maya.cmds surprises

        Args:
            name (str): New name for the node
            replace (tuple): Search and replace tuple i.e.: ('l_', 'r_') or ('l_', 'r_', 1)
                             Matches python string.replace() argument tuple logic
        """
        if name:
            mc.rename(self.name(short=False), name)
            return self

        if replace:
            mc.rename(self.name(short=False), self.name(short=True).replace(*replace))
            return self

    def delete(self):
        """
        Deletes the current node.
        Relies on node ID so no maya.cmds surprises
        """
        if not self:
            print 'Node n longer exists. Nothing to delete'
            return

        mc.delete(self.name())
        del(self)

    def duplicate(self):
        """
        Duplicates the node
        """
        return Nod(mc.duplicate(self.name())[0])

    def lock(self):
        """
        Lock current node
        """
        mc.lockNode(self.name(), l=True)

    def unlock(self):
        """
        Unlock current node
        """
        mc.lockNode(self.name(), l=False)

    def listRelatives(self, *args, **kwargs):
        """
        Utilizing maya.cmds.listRelatives() function
        """
        return map(Nod, mc.listRelatives(str(self), *args, **kwargs) or [])

    def children(self, **kwargs):
        """
        Utilizing maya.cmds.listRelatives() function with children flag True
        """
        return self.listRelatives(c=True, fullPath=True, **kwargs)

    def parent(self):
        """
        Returns immediate parent of the node

        Returns:
            (nod.nodetypes.base.Nod): Parent node
        """
        relatives = self.listRelatives(p=True, fullPath=True)
        if not relatives:
            return

        return relatives[0]

    def split(self):
        """
        Convenience method that operates on nod name string

        Returns:
            list: Results of string.split() operation
        """
        return self.name().split()

    def nodeType(self):
        """
        Convenience method to return Maya nodeType string on node
        Returns:
            str: Maya nodeType
        """
        return mc.nodeType(self.name())

    def shape(self):
        """
        Get first shape parented under the node

        Returns:
            nod.nodetypes.shape.NodShape
        """
        if not self.shapes():
            return

        return self.shapes()[0]

    def shapes(self):
        """
        Get list of all shapes parented under the node

        Returns:
            list
        """
        return map(Nod, mc.listRelatives(self.name(), fullPath=True, s=True) or [])

    def selectionList(self):
        """
        Convenience method to return maya.api selectionList with the current node
        Returns:
            maya.api.OpenMaya.MSelectionList
        """
        return om.MGlobal.getSelectionListByName(self.name())
