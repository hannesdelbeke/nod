
Creating Nod objects
----------------------

There are multiple ways of getting the Nod objects.

Casting existing nodes
````````````````````````

Name (string)
'''''''''''''''''

.. code-block:: python

	from nod import Nod
	joint = Nod('joint1')

:class:`nod.nodetypes.joint.NodJoint`

Pymel object (PyNode)
''''''''''''''''''''''''

.. code-block:: python

	import pymel.core as pm
	pmJoint = pm.joint()

	joint = Nod(pmJoint)

.. note::
	Even though **nod** supports casting the pymel nodes it does NOT require, neither imports any of the pymel modules

Selected
'''''''''''

.. code-block:: python

	import nod.cmds as nc
	nc.selected()
	# Returns <generator>

	nc.selectedAt(0)
	# Returns the first Nod object [index 0]

	nc.selectedAt(0)
	# Returns the last Nod object [index -1]

	nc.ls(sl=True)
	# Returns a list

Unpack multiple selected nodes

.. code-block:: python

	joint1, joint2, joint3 = nc.selected()


Creating new nodes
`````````````````````

Using createNode
'''''''''''''''''

.. code-block:: python

	import nod.cmds as nc
	null = nc.createNode('transform', n='foo_null')

:class:`nod.nodetypes.transform.NodTransform`

Using nod commands
'''''''''''''''''''''

This will look very familiar to the way nodes are created via **maya.cmds**. In fact :class:`nod.cmds` wraps up most of the commands keeping the **args** and **kwargs** as you know them. You can safely use :class:`nod.cmds` on both Nod objects as well as *string* names.

.. code-block:: python

	import nod.cmds as nc

	joint  = nc.joint(n='tenacious_jnt')
	group  = nc.group(n='tenacious_grp')
	locator = nc.spaceLocator(n='tenacious_loc')
	# etc.

.. note:: 

	The spaceLocator function returns the locators transform node
	as opposed to cmds were a tuple (transform, shape) is returned.

	:class:`nod.nodetypes.transform.NodTransform`

To access the locator's shape node simply call the method:

.. code-block:: python

	shape = locator.shape()








