Examples
==============


Imports
---------

.. code-block:: python

    from nod import Nod
    from nod import op

    import nod.cmds as nc

The power of working with :class:`nod` is that it's very easy to share short snippets of code that are very handy for day to day jobs.
This page will be expanded with such examples.


Useful snippets
----------------

Driven by distance between two selected nodes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. code-block:: python
	
	# Select two transforms
	op.distanceBetween(*nc.selected()) >> joint.tx

Rename nodes with non-unique names
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

	for node in nc.selected():
		node.rename(node.name(short=True) + '_SUFFIX')


Inserting an operation into existing network
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This would take quite a bit of rewrite when using ``maya.cmds`` but luckily this is where :class:`nod` comes super handy.

Let's say we have a setup and incoming connections to ``joint.ty`` channel, but we want to subtract an offset value.

.. code-block:: python

	joint.ty.input() - 10 >> joint.ty

The offset can also be a keyable attribute on a control node

.. code-block:: python

	joint.ty.input() - control.offset >> joint.ty

We could even add a new attribute to the control node if it didn't exist yet in a single line

.. code-block:: python

	joint.ty.input() - controladdAttr(ln='offset', k=True) >> joint.ty

Or perhaps we would like to clamp down the values of an existing incoming connections to (0, 10)

.. code-block:: python

	op.clamp(joint.ty.input(), min=0, max=10) >> joint.ty


Setting joint rotateOrder
^^^^^^^^^^^^^^^^^^^^^^^^^^

The *rotateOrder* channel supports the default int attributes but is extended to use *string* value as well, making it very intuitive to work with.

.. code-block:: python

	joint.rotateOrder.set('yxz')




