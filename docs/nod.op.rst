nod.op
=========

Nod *operators*

Operators are functions that create a part of the node operations network.
This is an extension of the default operators attached to **NodChannel** objects i.e. ``+ - * / **``


Example of an operator in a statement:

.. code-block:: python

	op.reversed(nodeA.v) >> nodeB.v



.. note::
	Refer to the docs and examples for more information.



.. toctree::

    nod.op.channels
    nod.op.nodes
    nod.op.measure


Module contents


.. automodule:: nod.op
    :members:
    :undoc-members:
    :show-inheritance:
